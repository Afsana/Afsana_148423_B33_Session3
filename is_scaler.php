<?php
function show_var($var)
{
    if (is_scalar($var))
    {
        echo $var;
    } else {
        var_dump($var);
    }
}
$pi = 3.1416;
$phones = array("Samsung", "Simphony", "Walton", "Oppo");
show_var($pi);
show_var($phones)
?>