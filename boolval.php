<?php
echo '"string": '.(boolval("string") ? 'true' : 'false')."\n";
echo "<br>";
echo '"0":      '.(boolval("0") ? 'true' : 'false')."\n";
echo "<br>";
echo '"1":      '.(boolval("1") ? 'true' : 'false')."\n";
echo "<br>";
echo '[1, 2]:   '.(boolval([1, 2]) ? 'true' : 'false')."\n";
echo "<br>";
echo '[]:       '.(boolval([]) ? 'true' : 'false')."\n";
echo "<br>";
echo 'stdClass: '.(boolval(new stdClass) ? 'true' : 'false')."\n";
?>